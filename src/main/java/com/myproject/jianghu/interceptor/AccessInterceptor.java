package com.myproject.jianghu.interceptor;

import com.myproject.jianghu.utils.HttpContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author gbs
 * @Description //TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
public class AccessInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpContextHolder.setCurrentRequest(request);
        HttpContextHolder.setCurrentResponse(response);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("------调用拦截器2---------");

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("------调用拦截器3---------");

    }
}
