package com.myproject.jianghu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.myproject.jianghu.**.dao")
public class JianghuApplication {

    public static void main(String[] args) {
        SpringApplication.run(JianghuApplication.class, args);
    }
}
