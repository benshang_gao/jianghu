package com.myproject.jianghu.dao;

import com.myproject.jianghu.entity.VoteNumberInstance;
import org.apache.ibatis.annotations.Param;


/**
 * 更新赞和踩的数量，获取赞和踩的数量
 * @author gaobenshang
 *
 */
public interface VoteDao {

	VoteNumberInstance getUpVote(@Param("objId") String objId);
	VoteNumberInstance getDownVote(@Param("objId") String objId);
	int updateUpVote(VoteNumberInstance vote);
	int updateDownVote(VoteNumberInstance vote);
	int insertNewVote(VoteNumberInstance newVote);
}
