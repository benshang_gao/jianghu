package com.myproject.jianghu.dao;

import java.util.List;

import com.myproject.jianghu.entity.CommentInstance;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;



public interface CommentsDao {

	List<CommentInstance> getCommentsByCoporation(@Param("corporationId") String corporation);
	int insertNewComment(CommentInstance newComment);
}
