package com.myproject.jianghu.dao;

import java.util.List;
import java.util.Map;

import com.myproject.jianghu.entity.QuestionInstance;
import org.apache.ibatis.annotations.Param;


public interface QuestionDao {

	int addNewQuestion(QuestionInstance question);
		
	List<QuestionInstance> getQuestionByAccount(@Param("account") String account);//差个账户提出的问题
	
	//发布新的问题
	int updateQuestion(@Param("question_id") String question_id);
	
	List<QuestionInstance> getAllQuestions();
	
	List<QuestionInstance> getAvailableQuestions(String longitude, String latitude);

	Map getQuestionById(@Param("question_id") String question_id);
}
