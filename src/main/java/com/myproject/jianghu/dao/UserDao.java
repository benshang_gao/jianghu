package com.myproject.jianghu.dao;

import java.util.List;

import com.myproject.jianghu.entity.UserInstance;
import org.apache.ibatis.annotations.Param;



public interface UserDao {

	List<UserInstance> getAllUser();
	UserInstance getUserByMobile(@Param("mobile") String mobile);
	UserInstance getUserByLoginName(@Param("name") String name);
	int insertNewUser(UserInstance newUser);
	List<UserInstance> getRelationShips(@Param("account") String account);
}
