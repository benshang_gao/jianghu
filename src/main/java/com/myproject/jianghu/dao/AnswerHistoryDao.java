package com.myproject.jianghu.dao;

import java.util.List;

import com.myproject.jianghu.entity.AnswerHistory;
import org.apache.ibatis.annotations.Param;

public interface AnswerHistoryDao {

	int addNewAnswer(AnswerHistory answer);
	
	List<AnswerHistory> getAnswerByAnswerer(@Param("account") String account);//查看自己答题记录
	
	List<AnswerHistory> getAnswerByQuestion(@Param("question_id") String question_id);//查看某个问题答题记录
	
}
