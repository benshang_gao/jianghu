package com.myproject.jianghu.dao;

import com.myproject.jianghu.entity.RoleAccessEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleAccessDao {

    public RoleAccessEntity getRoleListByUrl(@Param("url") String url);
    public RoleAccessEntity getRoleListByUser(@Param("uid") String uid);


}
