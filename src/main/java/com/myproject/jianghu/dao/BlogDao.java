package com.myproject.jianghu.dao;

import com.myproject.jianghu.entity.BlogInstance;

import java.util.List;


public interface BlogDao {

	int addNewBlog(BlogInstance newBlog);

	List<BlogInstance> getBlogList();
	
	
}
