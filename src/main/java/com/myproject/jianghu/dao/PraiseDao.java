package com.myproject.jianghu.dao;

import com.myproject.jianghu.entity.PraiseInstance;

/**
 * 点赞处理
 * @author gaobenshang
 *
 */
public interface PraiseDao {

	int addPraise(PraiseInstance newPraise);
	
	int cancelPraise(PraiseInstance praise);
}
