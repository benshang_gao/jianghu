package com.myproject.jianghu.dao;

import com.myproject.jianghu.entity.Catagray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CatagrayDao {

    void insertNewCatagray(Catagray newCatagray);

    List<Catagray> getCatagray();

    void deleteCatagrayById(@Param("cid")String cid);

}
