package com.myproject.jianghu.controller;

import com.myproject.jianghu.entity.BlogInstance;
import com.myproject.jianghu.service.BlogService;
import com.myproject.jianghu.service.CatagrayService;
import com.myproject.jianghu.utils.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/service/blog")
public class BlogController {

    Logger logger=LoggerFactory.getLogger(BlogController.class);

    @Autowired
    private CatagrayService catagrayService;
    @Autowired
    private BlogService blogService;
    
    @RequestMapping(value = "/addNewBlog")
    public Result addNewBlog(@RequestBody Map<String, String> newBlog,HttpServletRequest request){
    	System.out.println(newBlog);
        logger.info("创建新的博客");
        String user= request.getHeader("Authorization");
        newBlog.put("userId", "100010");
        blogService.addNewBlog(newBlog);
        Result result=new Result();
        result.setMessage("Success");
        return result;
    }

    @RequestMapping(value = "/getBlogs")
    public Result getBlogs(){
        Result result=new Result();
        List blogList=blogService.getBlogList();
        result.setMessage("Success");
        result.setData(blogList);
        return result;
    }

    @RequestMapping(value = "/getCatas")
    public Result getCatagray(){
        Result result=new Result();
        result.setMessage("Success");
        result.setData(catagrayService.getCatagray());
        return result;
    }
    /**
     * 获取我自己的博客
     * @return
     */
    @RequestMapping(value = "/getMyBlogs")
    public Result getMyBlogList(){
        Result result=new Result();
        result.setMessage("Success");
        result.setData(catagrayService.getCatagray());
        return result;
    }

    
    /**
     * 获取我
     * @return
     */
    @RequestMapping(value = "/getMyKeepList")
    public Result getMyKeepList(){
        Result result=new Result();
        result.setMessage("Success");
        result.setData(catagrayService.getCatagray());
        return result;
    }
}
