package com.myproject.jianghu.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myproject.jianghu.service.CommentService;

@Controller
@RequestMapping(value="/comments")
public class CommentController {

	@Resource
	private CommentService commentService;
	
	
	
	@RequestMapping(value="/getCommentsById/{objId}")
	@ResponseBody
	public List<?> getComments(HttpServletRequest request){
		String objId=request.getParameter("objId");
		return commentService.getCommentsByCoporationId(objId);
	}
	
	@RequestMapping(value="/addNewComment")
	@ResponseBody
	public Map<String, Object> addNewComment(HttpServletRequest request){
		Map<String, Object> resultMap=new HashMap<String, Object>();
		Map<String, String> newCommentParams=new HashMap<String, String>();
		newCommentParams.put("content", request.getParameter("commentContent"));
		newCommentParams.put("userId", request.getParameter("userId"));
		newCommentParams.put("corporation", request.getParameter("corporation"));

		try {
			int code=commentService.insertNewComment(newCommentParams);
			resultMap.put("statusCode", code);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resultMap;
	}
}
