package com.myproject.jianghu.controller;

import com.myproject.jianghu.utils.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/industry")
public class IndustryController {


    @ResponseBody
    @RequestMapping(value = "/getIndustries")
    public Result getIndustries(){
        Result result=new Result();
        List<Map<String,Object>> list=new ArrayList<>();
        Map<String,Object> i1=new HashMap<>();
        i1.put("id","1001");
        i1.put("name","计算机/软件");
        list.add(i1);
        Map<String,Object> i2=new HashMap<>();
        i2.put("id","1002");
        i2.put("name","建筑");
        list.add(i2);
        result.setData(list);
        result.setMessage("success");
        result.setStatusCode(200);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/getIndCates")
    public Result getIndCates(@RequestBody(required = false) Map<String,String>  param){
        List<Map<String,Object>> list=new ArrayList<>();
        if(param==null){
            param=new HashMap<>();
        }
        String indId=param.get("indId");
        if("1001".equals(indId)){
            Map<String,Object> i1=new HashMap<>();
            i1.put("id","1001");
            i1.put("name","Python");
            list.add(i1);
        }else if("1002".equals(indId)){
            Map<String,Object> i2=new HashMap<>();
            i2.put("id","1002");
            i2.put("name","Go");
            list.add(i2);
        }else {
            Map<String,Object> i2=new HashMap<>();
            i2.put("id","1003");
            i2.put("name","Java");
            list.add(i2);
        }
        Result result=new Result();
        result.setData(list);
        result.setMessage("success");
        result.setStatusCode(200);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "/getBooks")
    public Result getBooks(@RequestBody(required = false) Map<String,String>  param){
        List rsList=new ArrayList();
        Map<String,Object> map1=new HashMap<>();
        map1.put("name","Linux基础入门");
        map1.put("url","#");
        map1.put("zan",20210);
        rsList.add(map1);
        Result result=new Result();
        result.setStatusCode(200);
        result.setData(rsList);
        result.setMessage("success");
        return result;
    }

}
