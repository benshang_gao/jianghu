package com.myproject.jianghu.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.myproject.jianghu.service.ConsumerService.TestConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myproject.jianghu.utils.StaticUtil;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {


	@Autowired
	TestConsumer testService;

	@RequestMapping(value = "/newpage")
	public String getInfo() throws IOException {
		StaticUtil.generateFile("index", "E:\\index.html", new HashMap<>());
		return "/index";
	}

	@RequestMapping(value = "/hiview")
	public String getHiview() throws IOException {

		return "alist";
	}
	
	@RequestMapping(value = "/new")
	public String newFile(Map<String,String> map) throws IOException {

		map.put("id","2001110110110");
		return "new";
	}

	@RequestMapping(value = "/cangjingge")
	public String getCjg() throws IOException {
		StaticUtil.generateFile("cangjingge/cangjingge", "E:\\cangjingge.html", new HashMap<>());
		return "cangjingge/cangjingge";
	}

	@ResponseBody
	@RequestMapping(value = "/testAspect")
	public void testAspect(){
		testService.testRun();
	}
}
