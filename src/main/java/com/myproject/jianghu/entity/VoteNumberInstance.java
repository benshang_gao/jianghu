package com.myproject.jianghu.entity;
/**
 * 赞和踩的数量
 * @author gaobenshang
 *
 */
public class VoteNumberInstance {

	private String obj_Id;
	
	private int upVote;
	
	private int downVote;

	public String getObj_Id() {
		return obj_Id;
	}

	public void setObj_Id(String obj_Id) {
		this.obj_Id = obj_Id;
	}

	public int getUpVote() {
		return upVote;
	}

	public void setUpVote(int upVote) {
		this.upVote = upVote;
	}

	public int getDownVote() {
		return downVote;
	}

	public void setDownVote(int downVote) {
		this.downVote = downVote;
	}
	
}
