package com.myproject.jianghu.entity;
/**
 * 我的收藏
 * @author gaobenshang
 *
 */
public class KeepInstance {

	private String keepId;
	
	private String userId;
	
	private String objId;
	
	private String keepTime;

	public String getKeepId() {
		return keepId;
	}

	public void setKeepId(String keepId) {
		this.keepId = keepId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getObjId() {
		return objId;
	}

	public void setObjId(String objId) {
		this.objId = objId;
	}

	public String getKeepTime() {
		return keepTime;
	}

	public void setKeepTime(String keepTime) {
		this.keepTime = keepTime;
	}
	
	
}
