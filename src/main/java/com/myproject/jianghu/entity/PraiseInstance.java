package com.myproject.jianghu.entity;

public class PraiseInstance {

	private String praise_Id;
	
	private String user_Id;
	
	private String obj_Id;//点赞的对象

	public PraiseInstance(String id,String userId,String objId) {
		super();
		this.praise_Id=id;
		this.user_Id =userId;
		this.obj_Id=objId;		
	}
	public String getpraise_Id() {
		return praise_Id;
	}

	public void setpraise_Id(String praise_Id) {
		this.praise_Id = praise_Id;
	}

	public String getUser_Id() {
		return user_Id;
	}

	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}

	public String getObj_Id() {
		return obj_Id;
	}

	public void setObj_Id(String obj_Id) {
		this.obj_Id = obj_Id;
	}
	
	
	
}
