package com.myproject.jianghu.entity;

public class AnswerHistory {

	private String history_id;
	
	private String question_id;
	
	private String account;
	
	private String answer;
	
	private String anwertime;

	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}

	

	public String getHistory_id() {
		return history_id;
	}

	public void setHistory_id(String history_id) {
		this.history_id = history_id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAnwertime() {
		return anwertime;
	}

	public void setAnwertime(String anwertime) {
		this.anwertime = anwertime;
	}

	
	
	
}
