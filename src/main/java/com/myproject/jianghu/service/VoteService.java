package com.myproject.jianghu.service;

/**
 * 赞和踩
 * @author gaobenshang
 *
 */
public interface VoteService {


	int getUpVote(String objId);
	int getDownVote(String objId);
	int updateUpVote(String objId, int number);
	int updateDownVote(String objId, int number);
	int insertNewVote(String objId);
	
}
