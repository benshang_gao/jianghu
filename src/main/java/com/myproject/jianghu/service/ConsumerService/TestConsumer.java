package com.myproject.jianghu.service.ConsumerService;

import com.myproject.jianghu.service.TestService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class TestConsumer {

    @Reference
    TestService testService;

    public void testRun() {
      testService.testRun("高本尚");
    }
}
