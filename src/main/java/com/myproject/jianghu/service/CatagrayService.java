package com.myproject.jianghu.service;

import com.myproject.jianghu.entity.Catagray;

import java.util.List;

public interface CatagrayService {

    List<Catagray> getCatagray();
}
