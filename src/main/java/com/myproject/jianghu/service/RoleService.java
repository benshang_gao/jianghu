/**
 * 
 */
package com.myproject.jianghu.service;

import java.util.List;

/**
 * 
 * @author gaobenshang
 *
 */
public interface RoleService {

	List<?> getAllRole();
	List<?> getRolesByUser(String userId);
	List<?> getRoleByUrl(String url);
 }
