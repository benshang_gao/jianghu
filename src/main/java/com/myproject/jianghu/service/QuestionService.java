package com.myproject.jianghu.service;

import java.util.List;
import java.util.Map;

public interface QuestionService {

	int addNewQuestion(Map params);
	
	List<?>  getQuestionByAccount(String account);
	
	List getAllQuestions(String admin);
	List getAvailableQuestion(String longitude,String latitude);

	Map getQuestionById(String questionId);
}
