package com.myproject.jianghu.service.serviceImpl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.myproject.jianghu.dao.CommentsDao;
import com.myproject.jianghu.dao.VoteDao;
import com.myproject.jianghu.entity.CommentInstance;
import com.myproject.jianghu.entity.VoteNumberInstance;
import com.myproject.jianghu.service.CommentService;
import com.myproject.jianghu.utils.GenerateId;
import org.springframework.stereotype.Service;



@Service("commentService")
public class CommentServiceImpl implements CommentService {

	@Resource
	private CommentsDao commentsDao;
	
	@Resource
	private VoteDao voteDao;
	
	@Override
	public List<CommentInstance> getCommentsByCoporationId(String cId) {
		// TODO Auto-generated method stub
		System.out.println(commentsDao.getCommentsByCoporation(cId).size());
		return commentsDao.getCommentsByCoporation(cId);
	}

	@Override
	public int insertNewComment(Map<String,String> params) {
		// TODO Auto-generated method stub
		CommentInstance newComment=new CommentInstance();
		String newCommentId=GenerateId.getId();
		newComment.setComment_id(newCommentId);
		newComment.setContent((String)params.get("commentContent"));
		newComment.setPermission("Y");
		newComment.setUserid(params.get("userId"));
		newComment.setCorporation((String)params.get("corporation"));
		int i=commentsDao.insertNewComment(newComment);
		//添加评论数量
		VoteNumberInstance newVote=new VoteNumberInstance();
		newVote.setObj_Id(newCommentId);
		voteDao.insertNewVote(newVote);
		return i;
	}

}
