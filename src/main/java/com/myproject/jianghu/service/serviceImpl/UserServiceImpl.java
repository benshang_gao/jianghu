package com.myproject.jianghu.service.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.myproject.jianghu.dao.UserDao;
import com.myproject.jianghu.entity.UserInstance;
import com.myproject.jianghu.service.UserService;
import com.myproject.jianghu.utils.GenerateId;
import com.myproject.jianghu.utils.HexUtil;
import com.myproject.jianghu.utils.MD5Util;
import com.myproject.jianghu.utils.RSAUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service(value="userService")
public class UserServiceImpl implements UserService {
	private static Logger logger =LoggerFactory.getLogger(UserServiceImpl.class);
	
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public List getAllUser() {
		List<UserInstance> list =userDao.getAllUser();
		return list;
	}

	@Override
	public UserInstance getByName(String name) {
		// TODO Auto-generated method stub
		return userDao.getUserByLoginName(name);
	}

	@Override
	public int doRegister(Map<String, String> params) {
		// TODO Auto-generated method stub
		UserInstance newUser= new UserInstance();
		newUser.setName(params.get("username"));
		newUser.setMobile(params.get("mobile"));
		String password=params.get("password");
		newUser.setPassword(MD5Util.generate(password));
		newUser.setUserid(GenerateId.getId());
		return userDao.insertNewUser(newUser);
	}

	@Override
	public UserInstance getUserByMobile(String mobile) {
		// TODO Auto-generated method stub
		return userDao.getUserByMobile(mobile);
	}

	@Override
	public UserInstance validateLoginUser(String account, String password) {
		// TODO Auto-generated method stub
		String pw = "";
		try {
			byte[] en_result = HexUtil.hexStringToBytes(password);
			String de_result = RSAUtil.doDecrypt(RSAUtil.getKeyPair().getPrivate(),en_result);
			pw= new String(de_result);
			System.out.println(pw);
			UserInstance user = userDao.getUserByLoginName(account);		
			if(user!=null) {
				String realPassword=MD5Util.generate(pw);
				System.out.println("输入密码"+realPassword);
				System.out.println("数据库密码"+user.getPassword());
				if(user.getPassword().equals(realPassword)) {
					return user;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Cacheable(value="memcache",key="'user_relations_'+#p0")
	public Map getRelationShips(String account) {
		// TODO Auto-generated method stub
		logger.info("获取用户关系");
		Map<String, Object> resultMap=new HashMap<>();
		List<Map<String, String>> userList=new ArrayList<>();
		Map<String, String> map =new HashMap<>();
		map.put("name", "自由的小天王");
		map.put("head", "10001.jpg");//头像地址
		Map<String, String> map2 =new HashMap<>();
		map2.put("name", "利剑清风");
		map2.put("head", "logo.png");//头像地址
		Map<String, String> map3 =new HashMap<>();
		map3.put("name", "利剑清风");
		map3.put("head", "logo.png");//头像地址
		userList.add(map);
		userList.add(map2);
		userList.add(map3);
		resultMap.put("nodes", userList);
		List<Object> relationList=new ArrayList<>();
		Map<String, Object> relation =new HashMap<>();
		relation.put("source", 0);
		relation.put("target", 1);
		relation.put("relation", "挚友");//头像地址
		Map<String, Object> relation2 =new HashMap<>();
		relation2.put("source", 1);
		relation2.put("target", 2);
		relation2.put("relation", "挚友");//头像地址
		relationList.add(relation);
		relationList.add(relation2);
		resultMap.put("edges", relationList);
		logger.info("获取成功");

		return resultMap;
	}

}
