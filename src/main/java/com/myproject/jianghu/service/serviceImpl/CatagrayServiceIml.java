package com.myproject.jianghu.service.serviceImpl;

import com.myproject.jianghu.dao.CatagrayDao;
import com.myproject.jianghu.entity.Catagray;
import com.myproject.jianghu.service.CatagrayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("catagrayService")
public class CatagrayServiceIml implements CatagrayService {

    @Autowired
    private CatagrayDao catagrayDao;


    @Override
    public List<Catagray> getCatagray() {
        return catagrayDao.getCatagray();
    }
}
