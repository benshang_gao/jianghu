package com.myproject.jianghu.service.serviceImpl;

import com.myproject.jianghu.dao.VoteDao;
import com.myproject.jianghu.entity.VoteNumberInstance;
import com.myproject.jianghu.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("voteService")
public class VoteServiceImpl implements VoteService {

	@Autowired
	VoteDao voteDao;
	
	@Override
	public int getUpVote(String objId) {
		// TODO Auto-generated method stub
		VoteNumberInstance vote=voteDao.getUpVote(objId);
		if(vote==null) {
			return 0;
		}
		return vote.getUpVote();
	}

	@Override
	public int getDownVote(String objId) {
		// TODO Auto-generated method stub
		VoteNumberInstance vote=voteDao.getUpVote(objId);
		if(vote==null) {
			return 0;
		}
		return vote.getUpVote();
	}

	@Override
	public int updateUpVote(String objId, int number) {
		// TODO Auto-generated method stub
		VoteNumberInstance vote=new VoteNumberInstance();
		vote.setObj_Id(objId);
		vote.setUpVote(number);
		
		return voteDao.updateUpVote(vote);
	}

	@Override
	public int updateDownVote(String objId, int number) {
		// TODO Auto-generated method stub
		VoteNumberInstance vote=new VoteNumberInstance();
		vote.setObj_Id(objId);
		vote.setDownVote(number);
		
		return voteDao.updateUpVote(vote);
	}

	@Override
	public int insertNewVote(String objId) {
		// TODO Auto-generated method stub
		VoteNumberInstance newVote=new VoteNumberInstance();
		newVote.setObj_Id(objId);
		
		return voteDao.insertNewVote(newVote);
	}

}
