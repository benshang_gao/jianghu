package com.myproject.jianghu.service.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.myproject.jianghu.dao.BlogDao;
import com.myproject.jianghu.entity.BlogInstance;
import com.myproject.jianghu.service.BlogService;
import com.myproject.jianghu.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;

@Service
public class BlogServiceImpl implements BlogService {

	private static Logger logger=LoggerFactory.getLogger(BlogServiceImpl.class);

	@Autowired
	protected BlogDao blogDao;

	@Override
	public Map<String, Object> addNewBlog(Map parmas) {
		// TODO Auto-generated method stub
		Map<String, Object> resultMap=new HashMap<>();
		BlogInstance newBlog=new BlogInstance();
//		String userId=(String) MemCachedUtil.get((String)parmas.get("userToken"));
		newBlog.setAuthor("");
//		newBlog.setBlog_id(GenerateId.getId());
		//将id和标签关联起来
		String tags=(String)parmas.get("tags");
		List<String> tagList = JSONArray.parseObject(tags, List.class);
		
		//添加标签到数据库
		newBlog.setContent(parmas.get("content").toString());
		newBlog.setTitle((String)parmas.get("title"));
		String currentDateTime=DateUtil.getDateTime();
		newBlog.setDatetime(currentDateTime);
		int result = blogDao.addNewBlog(newBlog);
		resultMap.put("resultCode", result);
		return resultMap;
	}

	@Override
	public List<Object> getBlogList() {
		// TODO Auto-generated method stub
		List list=blogDao.getBlogList();
		return list;
	}

}
