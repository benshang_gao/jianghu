package com.myproject.jianghu.service.serviceImpl;

import java.util.List;

import com.myproject.jianghu.dao.RoleAccessDao;
import com.myproject.jianghu.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleAccessDao roleAccessDao;

	@Override
	public List<?> getAllRole() {

		return null;
	}

	@Override
	public List<?> getRolesByUser(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getRoleByUrl(String url) {
		return roleAccessDao.getRoleListByUrl(url).getRoleList();
	}

}
