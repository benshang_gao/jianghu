package com.myproject.jianghu.service.serviceImpl;

import java.util.List;
import java.util.Map;

import com.myproject.jianghu.dao.QuestionDao;
import com.myproject.jianghu.entity.QuestionInstance;
import com.myproject.jianghu.service.QuestionService;
import com.myproject.jianghu.utils.GenerateId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service("questionService")
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	QuestionDao questionDao;

	@Override
	public int addNewQuestion(Map params) {
		QuestionInstance newQuestion=new QuestionInstance();
		String userToken=(String)params.get("userToken");
//		String account=(String)MemCachedUtil.get(userToken);
		String questionId=GenerateId.getId();
		newQuestion.setQuestion_id(questionId);
//		newQuestion.setAccount(account);
		newQuestion.setDescription((String)params.get("description"));
		newQuestion.setAccount((String)params.get("account"));
		newQuestion.setQuestion((String)params.get("question"));
		newQuestion.setRightkey((String)params.get("rightkey"));
		newQuestion.setSelect1((String)params.get("select1"));
		newQuestion.setSelect2((String)params.get("select2"));
		newQuestion.setSelect3((String)params.get("select3"));
		newQuestion.setSelect4((String)params.get("select4"));
		
		return questionDao.addNewQuestion(newQuestion);
	}

	@Override
	public List getQuestionByAccount(String account) {
		// TODO Auto-generated method stub
		List list=questionDao.getQuestionByAccount(account);
		return list;
	}

	@Override
	public List getAllQuestions(String admin) {
		//验证用户角色
		
		return questionDao.getAllQuestions();
	}

	@Override
	public List getAvailableQuestion(String longitude,String latitude) {
		//位置判断
		questionDao.getAvailableQuestions(longitude,latitude);
		return null;
	}

	@Override
	@Cacheable(value="questionAttr",key="#questionId")
	public Map getQuestionById(String questionId) {
		// TODO Auto-generated method stub
		
		return questionDao.getQuestionById(questionId);
	}
	
	
}
