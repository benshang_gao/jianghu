package com.myproject.jianghu.service.serviceImpl;

import com.myproject.jianghu.dao.PraiseDao;
import com.myproject.jianghu.entity.PraiseInstance;
import com.myproject.jianghu.service.PraiseService;
import com.myproject.jianghu.utils.GenerateId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("grateService")
public class PraiseServiceImpl implements PraiseService {

	@Autowired
	PraiseDao grateDao;
	@Override
	public boolean addPraise(String userId, String objId) {
		// TODO Auto-generated method stub
		String newGrateId =GenerateId.getId();
		PraiseInstance newGrate=new PraiseInstance(newGrateId, userId, objId);
		int i= grateDao.addPraise(newGrate);
		if(i==1) {
			//点赞成功后，更新点赞数量
			
			return true;
		}
		return false;
	}

}
