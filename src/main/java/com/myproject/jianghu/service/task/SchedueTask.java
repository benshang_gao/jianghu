package com.myproject.jianghu.service.task;

import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Lazy(false)
@Service
@EnableScheduling
public class SchedueTask {

	@Scheduled(cron = "0/5 * * * * ?")
	public void runTask() {
		
	}
}
