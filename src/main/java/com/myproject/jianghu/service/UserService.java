package com.myproject.jianghu.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.myproject.jianghu.entity.UserInstance;

public interface UserService {
	public List getAllUser();
	public UserInstance getByName(String name);
	int doRegister(Map<String,String> params);
	UserInstance getUserByMobile(String mobile);
	UserInstance validateLoginUser(String account,String password);
	Map getRelationShips(@Param("account")String account);
}
