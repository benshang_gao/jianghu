package com.myproject.jianghu.service;

import java.util.List;
import java.util.Map;

import com.myproject.jianghu.entity.CommentInstance;

public interface CommentService {

	List<CommentInstance> getCommentsByCoporationId(String cId);
	
	int insertNewComment(Map<String,String> newComment);
}
