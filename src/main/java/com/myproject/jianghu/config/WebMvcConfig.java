package com.myproject.jianghu.config;

import com.myproject.jianghu.interceptor.AccessInterceptor;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author chengpunan
 * @Description //TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有api请求，有@NoCheck注解的方法直接通过
        registry.addInterceptor(authenticationInterceptor()).addPathPatterns("/loginPage/**");
    }

    @Bean
    public AccessInterceptor authenticationInterceptor() {
        return new AccessInterceptor();
    }
}
