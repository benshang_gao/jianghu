package com.myproject.jianghu.annotation;

import com.myproject.jianghu.entity.RoleEntity;
import com.myproject.jianghu.service.RoleService;
import com.myproject.jianghu.service.UserService;
import com.myproject.jianghu.utils.HttpContextHolder;
import com.myproject.jianghu.utils.JwtTokenUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.relation.RoleList;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author chengpunan
 * @Description //TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
@Component
@Aspect
public class AccessCheckAspect {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    JwtTokenUtils jwtTokenUtils;


    @Before("@annotation(AccessCheck)")
    public void doCheck(JoinPoint point){
        List list=userService.getAllUser();
        System.out.println(list);
        System.out.println("-----------");
    }

    @Around("@annotation(AccessCheck)")
    public Object interceptor(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request =HttpContextHolder.getCurrentRequest();
        String token =  request.getHeader("access_token");//获取acesstoken
        String url = request.getServletPath();
        List<RoleEntity> list= jwtTokenUtils.getRoleListFromToken(token);
        List needList = roleService.getRoleByUrl(url);
        System.out.println(needList);
        if(needList.contains(list)){
            return pjp.proceed();
        }
        return null;
    }

    @After("@annotation(AccessCheck)")
    public void afterSwitchDS(JoinPoint point){
        System.out.println("------调用-----");
    }

}
