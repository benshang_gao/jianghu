package com.myproject.jianghu.annotation;

import com.myproject.jianghu.entity.RoleEntity;
import com.myproject.jianghu.service.RoleService;
import com.myproject.jianghu.service.UserService;
import com.myproject.jianghu.utils.HttpContextHolder;
import com.myproject.jianghu.utils.JwtTokenUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author chengpunan
 * @Description //TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
@Component
@Aspect
public class TestAspect {


    @Pointcut("execution(* com.myproject.jianghu.service.TestService.testRun(..))")
    public void log(){

    }
    @Before("log()")
    public void doCheck(JoinPoint point){

        System.out.println("-----------");
    }

    @Around("log()")
    public Object interceptor(ProceedingJoinPoint pjp) throws Throwable {
        if(true){
            return pjp.proceed();
        }
        return null;
    }

    @After("log()")
    public void afterSwitchDS(JoinPoint point){
        System.out.println("------调用-----");
    }

}
