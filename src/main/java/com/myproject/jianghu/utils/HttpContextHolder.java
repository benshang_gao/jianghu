package com.myproject.jianghu.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author gbs
 * @Description //TODO
 * @Date $ $
 * @Param $
 * @return $
 **/
public class HttpContextHolder {
    private static ThreadLocal<HttpServletRequest>requestThreadLocal=new ThreadLocal<>();
    private static ThreadLocal<HttpServletResponse>responseThreadLocal=new ThreadLocal<>();
    public static HttpServletRequest getCurrentRequest() {
        return requestThreadLocal.get();
    }
    public static void setCurrentRequest(HttpServletRequest request) {
        requestThreadLocal.set(request);
    }
    public static HttpServletResponse getCurrentResponse() {
        return responseThreadLocal.get();
    }
    public static void setCurrentResponse(HttpServletResponse response) {
        responseThreadLocal.set(response);
    }

}
