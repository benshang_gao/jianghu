package com.myproject.jianghu.utils;
/**
 * 
 * @author Administrator
 *
 */
public class HexUtil {
	/** * 16进制 To byte[] 
	 * 
	 * 
	* @param hexString * @return byte[] 
	* 
	* */

    public static byte[] hexStringToBytes(String hexString) {

        if (hexString == null || hexString.equals("")) {

            return null;

        }

        hexString = hexString.toUpperCase();

        int length = hexString.length() / 2;

        char[] hexChars = hexString.toCharArray();

        byte[] d = new byte[length];

        for (int i = 0; i < length; i++) {

            int pos = i * 2;

            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));

        }

        return d;

    }

 

    /** * Convert char to byte * @param c char * @return byte */

    private static byte charToByte(char c) {

        return (byte) "0123456789ABCDEF".indexOf(c);

    }
}
