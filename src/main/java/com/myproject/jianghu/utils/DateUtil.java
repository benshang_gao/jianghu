package com.myproject.jianghu.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;

/**
 * 时间转换处理类
 * @author gaobenshang
 *
 */
public class DateUtil {

    // 默认日期格式
    public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";

    // 默认日期时间格式
    public static final String DATETIME_DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    //时间格式
    public static final String TIME_DEFAULT_FORMAT = "HH:mm:ss";

    // 日期格式化
    private static DateTimeFormatter dateFormat = null;

    // 时间格式化
    private static DateTimeFormatter dateTimeFormat = null;

    private static DateTimeFormatter timeFormat = null;

    private static Calendar gregorianCalendar = null;

    static {
        dateFormat = DateTimeFormatter.ofPattern(DATE_DEFAULT_FORMAT);
        dateTimeFormat = DateTimeFormatter.ofPattern(DATETIME_DEFAULT_FORMAT);
        timeFormat = DateTimeFormatter.ofPattern(TIME_DEFAULT_FORMAT);
    }

    /**
     * 日期格式化yyyy-MM-dd
     * 
     * @param date
     * @return
     */
    public static LocalDate formatDate(String date, String format) {
        if(StringUtils.isNotBlank(format)) {
        	return (LocalDate) DateTimeFormatter.ofPattern(format).parse(date);
        }        
        return LocalDate.now();
    }

    /**
     * 日期格式化yyyy-MM-dd
     * 
     * @param date
     * @return
     */
    public static String getDateFormat(LocalDate date) {
        return date.format(dateFormat);
    }

    /**
     * 日期格式化yyyy-MM-dd HH:mm:ss
     * 
     * @param
     * @return
     */
    public static String getDateTimeFormat(LocalDateTime dateTime) {
        return dateTimeFormat.format(dateTime);
    }

    /**
     * 时间格式化
     * 
     * @param
     * @return HH:mm:ss
     */
    public static String getTimeFormat(LocalTime time) {
        return time.format(timeFormat);
    }

    /**
     * 日期格式化
     * 
     * @param date
     * @param
     * @return
     */
    public static String getDateFormat(LocalDate date, String format) {
    	if(date==null) {
    		date =LocalDate.now();
    	}
        if (StringUtils.isNotBlank(format)) {
            return date.format(DateTimeFormatter.ofPattern(format));
        }else {
            return date.format(dateFormat);
		}
    }

    /**
     * 日期格式化
     * 
     * @param date
     * @return
     */
    public static LocalDate stringToDate(String date,String str) {
    	if(StringUtils.isNotBlank(date)) {
    		if(StringUtils.isNotBlank(date)) {
    			DateTimeFormatter.ofPattern(str).parse(date);
    		}else {
				dateFormat.parse(date);
			}
    	}
        return LocalDate.now();
    }

    /**
     * 时间格式化
     * 
     * @param date
     * @return
     */
    public static LocalDateTime getDateTimeFormat(String date,String format) {
       if(StringUtils.isNotBlank(date)) {
    	   if(StringUtils.isNotBlank(format)) {
    		   return (LocalDateTime) DateTimeFormatter.ofPattern(format).parse(date);
    	   }else {
    		   return (LocalDateTime)dateTimeFormat.parse(date);
    	   }
       }
        return LocalDateTime.now();
    }


    public static String getDateTime() {
    	LocalDateTime dateTime =LocalDateTime.now();
    	return getDateTimeFormat(dateTime);
    }
    /**
     * 获取当前年
     * 
     * @return
     */
    public static int getCurrentYear() {
        LocalDate date = LocalDate.now();
        int year =date.get(ChronoField.YEAR);
        return year;
    }

    /**
     * 获取当前月份
     * 
     * @return
     */
    public static int getCurrentMonth() {
    	  LocalDate date = LocalDate.now();
          int month =date.get(ChronoField.MONTH_OF_YEAR);
          return month;
    }

    /**
     * 获取当月天数
     * 
     * @return
     */
    public static int getMonthDay() {
    	 LocalDate date = LocalDate.now();
    	 int days=date.lengthOfMonth();
        return days;
    }
    /**
     * 计算两个日期相差多少天
     * @param startDate
     * @param endDate
     * @return
     */
    public static long getBettwenDays(LocalDate startDate,LocalDate endDate) {
    	long days = startDate.until(endDate, ChronoUnit.DAYS);
    	return days;
    }
    
    /**
     * 计算两个日期相差多少个月
     * @param startDate
     * @param endDate
     * @return
     */
    public static long getBettwenMonths(LocalDate startDate,LocalDate endDate) {
    	long days = startDate.until(endDate, ChronoUnit.MONTHS);
    	return days;
    }
}