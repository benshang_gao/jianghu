package com.myproject.jianghu.utils;

public class Constants {
	
	public static final int PAGE_DEFAULT_NUM = 10;//分页的默认大小
	public static String MD5_SALT ="G123b456S?!";//MD5加密默认盐值
	public static final String USERTOKEN="USERTOKEN";//登录的cookie名称userToken
	public static final String LOGIN_REQUIRED_LINK="login_required_url";//缓存，需要登录的连接名称
	
	public static final int STATUS_CODE_SUCCESS=1;//Controller执行成功的返回值
	
	public static final int STATUS_CODE_FAILED=0;//Controller执行成功的返回值


}
