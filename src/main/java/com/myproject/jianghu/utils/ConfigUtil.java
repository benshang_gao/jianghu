package com.myproject.jianghu.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * 配置文件操作类
 * @author Administrator
 *
 */
public class ConfigUtil {

	/**
	 * 从配置文件中读取属性值
	 * @param filePath
	 * @param attrName
	 * @return
	 */
	public static String getPropertiesValue(String filePath,String attrName) {
		String attrValue="";
		try {
			PropertiesConfiguration config = new PropertiesConfiguration(filePath);
			attrValue = config.getString(attrName,"");//若没有查询到该属性值，则返回默认值
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		return attrValue;
		
	}
	/**
	 * 向配置文件中添加新的配置属性
	 * @param filePath
	 * @param attrName
	 * @param attrValue
	 */
	public static void setPropertiesValue(String filePath,String attrName,String attrValue) {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration(filePath);
			config.setProperty(attrName, attrValue);
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

	}
}
