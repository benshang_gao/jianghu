package com.myproject.jianghu.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
/**
 * 模板引擎，利用Thymleaf生成静态文件
 * @author Administrator
 *
 */
public class StaticUtil {
	
	public static void generateFile(String templateName,String path,Map<String, Object> params) throws IOException {
		//构造模板引擎
	    ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
	    resolver.setPrefix("templates/");//模板所在目录，相对于当前classloader的classpath。
	    resolver.setSuffix(".html");//模板文件后缀
	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(resolver);

	    //构造上下文(Model)
	    Context context = new Context();
	    params.forEach((k,v)->{
	    	 context.setVariable(k,v);
	    });

	    //渲染模板
	    FileWriter write = new FileWriter(path+".html");
	    templateEngine.process(templateName, context, write);
	    write.close();
	}
	

}
