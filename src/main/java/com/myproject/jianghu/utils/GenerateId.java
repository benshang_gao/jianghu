package com.myproject.jianghu.utils;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
/**
 * 产生数据ID
 * @author gaobenshang
 *
 */
public class GenerateId {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getId("Grate",""));
	}
	public static String getId() {
		return UUID.randomUUID().toString();
	}
	public static String getId(String prefix,String postfix) {
		StringBuffer id = new StringBuffer();
		if(StringUtils.isNotBlank(prefix)) {
			id.append(prefix+"-");
		}
		id.append(getId().replaceAll("-", ""));
		if(StringUtils.isNotBlank(postfix)) {
			id.append("-"+postfix);
		}
		return id.toString();
	}

}
