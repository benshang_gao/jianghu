function aa(url,params,type){
    $.ajax({
        async: false,
        type: type,
        url:url,
        contentType : "application/json; charset=utf-8",
        data:params,
        success: function (data) {

        },
        error: function () {

        }
    })

}
$(function () {
    var industries=[],indcats=[],books=[];
    industries=getIndustries();
    indcats=getIndCates();
    books=getBooks();
    var vue=new Vue({
        el:'.content',
        data:{
            industries:industries,
            indCates:indcats
        },
        methods:{
            getIndCates:function(IndId) {
                this.indCates=getIndCates(IndId);
            },
            refreshBooks:function(event){
                Vue.set(books,getBooks(event.target.id));
            }
        }

    });
    var vue2=new Vue({
        el:'#myTabContent',
        data:{
            books:books
        }

    })
    function getIndustries() {
        var url='/jianghu/industry/getIndustries';
        var result=doRequest(url,null,'GET');
        return result.data;
    }
    function getIndCates(indId) {
        var url='/jianghu/industry/getIndCates';
        var param={};
        param.indId=indId;
        var json=JSON.stringify(param);
        return doRequest(url,json,'POST').data
    }
    function  getBooks(catId) {
        var url='/jianghu/industry/getBooks';
        var param={};
        param.catId=catId;
        var json=JSON.stringify(param);
        return doRequest(url,json,'POST').data
    }
    function doRequest(url,param,type) {
        var rs;
        $.ajax({
            async: false,
            type: type,
            data:param,
            url:url,
            contentType : "application/json; charset=utf-8",
            success: function (result) {
                rs=result;
            },
            error: function () {
                
            }
        });
        return rs;
    }

    function ss (id) {
        $.ajax({
            async: false,
            type: 'GET',
            url:'/jianghu/industry/getIndustriesCat',
            contentType : "application/json; charset=utf-8",
            success: function (result) {
                way.set('industry.list',result.data);
            },
            error: function () {

            }
        });
    }
})